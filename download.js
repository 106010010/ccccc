function downloadImg(){
    var image = canvas.toDataURL('image/png');
    download_btn.href = image;
}

var imageLoader = document.getElementById('upload');
    imageLoader.addEventListener('change', uploadImg, false);



function uploadImg(e){
    //let color = strokeColor.value;
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            context.drawImage(img,0,0);
            context.strokeStyle = Color;
            context.fillStyle = Color;
            context.lineWidth = radius*2;
            Push();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}