var size_font;
var font;
var size;
var Input = false;

//var SELECT = document.getElementById("fonts");
//var fontSelected = SELECT.selectedIndex;

canvas.addEventListener('click', InputText);





//options[fontSelected]

function setF(newF){
    if(newF < minF){
        newF = minF;
    }else if(newF > maxF){
        newF = maxF;
    }
    size = newF;
    //context.lineWidth = radius*2;

    Fspan.innerHTML = size;
}

var minF = 12;
var maxF = 52;
var defaultF = 16;
var Finterval = 4;
var Fspan = document.getElementById('Fval');
var decF = document.getElementById('decreaseF');
var incF = document.getElementById('increaseF');
setF(defaultF);

decF.addEventListener('click', function(){
    setF(size-Finterval)
});

incF.addEventListener('click', function(){
    setF(size+Finterval)
});



function InputText(e) {
    if (currentTool === 'text') {

        if(document.getElementById("fonts").value === 'serif'){
            font = 'serif';
            
        }else if(document.getElementById("fonts").value === 'sans-serif'){
            font = 'sans-serif';
        }else if(document.getElementById("fonts").value === 'cursive'){
            font = 'cursive';
            console.log("font");
        }else if(document.getElementById("fonts").value === 'monospace'){
            font = 'monospace';
        }

        

size_font = size + "pt " + font;


        if (Input) return;
        addInput(e.clientX, e.clientY);
    }

}

function addInput(x, y) {
    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;

    document.body.appendChild(input);

    input.focus();

    Input = true;
    Push();
}

function handleEnter(e) {
    var keyCode = e.keyCode;

    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10),
            parseInt(this.style.top, 10));
        document.body.removeChild(this);
        Input = false;
    }
}

function drawText(txt, x, y) {
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = size_font;
    context.fillText(txt, x - 4, y - 4);
}


